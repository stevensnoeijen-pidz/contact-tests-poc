module.exports = {
    "testEnvironment": "node",
    preset: "ts-jest",
    "roots": [
        "<rootDir>/src"
    ],
    "transform": {
        "^.+\\.tsx?$": "ts-jest"
    },
    moduleFileExtensions: [
        'js',
        'jsx',
        'json',
        'vue',
        'ts',
        'tsx'
    ],
    testMatch: [
        '**/*.(spec|contract).(ts|tsx)'
    ]
};
