import { INumberToWordsResponse } from './models/INumberToWordsResult';
import axios from "axios";
import { j2xParser, getTraversalObj, convertToJson } from "fast-xml-parser";

interface INumberConversionClientProps {
    baseUrl?: string;
}

export class NumberConversionClient {
    private readonly baseUrl: string = 'https://www.dataaccess.com/webservicesserver';

    constructor(props?: INumberConversionClientProps){
        this.baseUrl = props?.baseUrl || this.baseUrl;
    }

    public async numberToWords(ubiNum: number): Promise<string> {
        const response = await axios.request<string>({
            method: 'POST',
            baseURL: this.baseUrl,
            url: '/NumberConversion.wso',
            headers: {
                'Content-Type': 'application/soap+xml; charset=utf-8',
            },
            data: `<?xml version="1.0" encoding="utf-8"?>
            <soap12:Envelope xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">
              <soap12:Body>
                <NumberToWords xmlns="http://www.dataaccess.com/webservicesserver/">
                  <ubiNum>${ubiNum}</ubiNum>
                </NumberToWords>
              </soap12:Body>
            </soap12:Envelope>`,
        });

        const xml = getTraversalObj(response.data);
        const json = convertToJson(xml, {
            attributeNamePrefix: '_',
            textNodeName: '__text',
        }) as INumberToWordsResponse;

        return json['soap:Envelope']['soap:Body']['m:NumberToWordsResponse']['m:NumberToWordsResult'];
    }
}