import { PetResource } from './PetResource';

interface IPetStoreClientProps {
    baseUrl?: string;
}

export class PetStoreClient {
    public readonly pet: PetResource;

    constructor(props?: IPetStoreClientProps) {
        this.pet = new PetResource(props);
    }
}