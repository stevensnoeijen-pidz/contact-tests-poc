import { Pet } from './models/Pet';
import axios from 'axios';
import { Status } from './models/Status';

interface IPetResourceProps {
    baseUrl?: string;
}

export class PetResource {
    private readonly baseUrl: string = 'https://petstore.swagger.io/v2/pet';

    constructor(props?: IPetResourceProps){
        this.baseUrl = props?.baseUrl || this.baseUrl;
    }

    public async findByStatus(status: Status): Promise<Pet[]> {
        const response = await axios.request<Pet[]>({
            method: 'GET',
            baseURL: this.baseUrl,
            url: '/findByStatus',
            params: {
                status: status,
            },
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
            }
        });

        return response.data;
    }
}