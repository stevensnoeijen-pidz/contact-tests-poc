import { Pet } from './models/Pet';
import { PetStoreClient } from './PetStoreClient';
import { pactWith } from 'jest-pact';
import { InteractionObject, Matchers, Pact, RequestOptions, ResponseOptions } from '@pact-foundation/pact';

describe('PetStoreClient native', () => {
    describe('pet', () => {
        const client = new PetStoreClient();
        test('findByStatus', async () => {
            const pets = await client.pet.findByStatus('available');
            
            expect(pets.length).toBeGreaterThan(1);
            expect(pets[0].id).not.toBeNull();
            expect(pets[0].category).not.toBeNull();
            expect(pets[0].name).not.toBeNull();
            expect(pets[0].photoUrls).not.toBeNull();
            expect(pets[0].tags).not.toBeNull();
        });
    });
});

pactWith(
    {
        consumer: "PetStoreClient", 
        provider: "petstore.swagger.io",
    },
    (provider) => {
        describe('PetStoreClient pact', () => {
            describe("pet", () => {
                describe('findByStatus', () => {
                    const findByStatus_data = {
                        "id": Matchers.integer(),
                        "category": {
                            "id": 1,
                            "name": "test bobbi"
                        },
                        "name": "test_bobbi",
                        "photoUrls": [
                            "Lorem magna aute\",\"et occaecat dolor eiusmod"
                        ],
                        "tags": [
                            {
                            "id": -26175073,
                            "name": "test_bobbi"
                            },
                            {
                            "id": -64567941,
                            "name": "in laborum"
                            }
                        ],
                        "status": "available",
                    };
            
                    const findByStatusRequest: RequestOptions = {
                        method: "GET",
                        path: "/findByStatus",
                        headers: {
                            Accept: "application/json",
                        },
                    };

                    const findByStatusResponseSuccess: ResponseOptions = {
                        status: 200,
                        headers: {
                            "Content-Type": "application/json",
                        },
                        body: Matchers.eachLike(findByStatus_data),
                    };

                    beforeEach(() => {
                        const interaction: InteractionObject = {                        
                            uponReceiving: "findByStatus with status available",
                            state: "list of pets",
                            withRequest: findByStatusRequest,
                            willRespondWith: findByStatusResponseSuccess,

                        }
                        return provider.addInteraction(interaction)
                    })
            
                    test("with status available", async () => {
                        const client = new PetStoreClient({
                            baseUrl: provider.mockService.baseUrl,
                        });

                        const pets = await client.pet.findByStatus('available');
                        expect(pets).toEqual([findByStatus_data]);
                    });
                });
            });
        });
    });