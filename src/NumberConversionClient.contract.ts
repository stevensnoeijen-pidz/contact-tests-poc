import { NumberConversionClient } from './NumberConversionClient';

describe('NumberConversionClient', () => {
    describe('numberToWords', () => {
        test('when number given', async () => {
            const client = new NumberConversionClient();
            const word = await client.numberToWords(1);

            expect(word).toEqual('one');
        });

        test('when incorrect number given', async () => {
            const client = new NumberConversionClient();
            const word = await client.numberToWords(-1);

            expect(word).toEqual('number too large');
        });
    });
});