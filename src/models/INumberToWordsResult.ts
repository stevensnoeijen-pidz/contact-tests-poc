export interface INumberToWordsResponse {
    'soap:Envelope': {
      'soap:Body': {
        'm:NumberToWordsResponse': {
          'm:NumberToWordsResult': string;}
        }
      }
    }
}